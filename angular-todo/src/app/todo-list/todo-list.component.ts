import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { TodoService, ITodoItem,  } from '../todo.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})

export class TodoListComponent implements OnInit, OnDestroy {
  constructor(private todoService: TodoService) { }

  protected todoSubscribe: Subscription;
  protected todos: ITodoItem[];

  ngOnInit() {
    this.todoSubscribe = this.todoService.getTodosSubscription(
      this.handleTodosUpdate.bind(this),
      this.handleTodosUpdateError.bind(this)
    );
  }

  ngOnDestroy() {
    if (this.todoSubscribe) {
      this.todoSubscribe.unsubscribe();
    }
  }

  protected handleTodosUpdate(data: ITodoItem[]) {
    this.todos = data;
  }

  protected handleTodosUpdateError(error: any) {
    console.error('Unexpected error while updating todos', error);
  }


  protected handleToggleTodo(index: number) {
    this.todoService.toggleTodo(index);
  }

  protected handleAddTodo() {
    const todo = window.prompt('Enter your Todo name');

    if (!todo || !todo.length) {
      return false;
    }

    this.addTodo({ name: todo });
  }

  protected addTodo(todo: ITodoItem) {
    this.todoService.addTodo(todo);
  }

  protected handleRemoveTodo(index: number) {
    if (window.confirm('Are you sure you want to remove this todo?')) {
      this.removeTodo(index);
    }
  }

  protected removeTodo(index: number) {
    this.todoService.removeTodo(index);
  }

  protected getTodos(): ITodoItem[] {
    return this.todoService.getTodos();
  }

}
