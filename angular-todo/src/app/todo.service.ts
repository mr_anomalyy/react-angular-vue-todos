import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface ITodoItem {
  name: string;
  isDone?: boolean;
}

@Injectable({
  providedIn: 'root'
})

export class TodoService {
  constructor() { }

  protected todos: BehaviorSubject<ITodoItem[]> = new BehaviorSubject<ITodoItem[]>([
    { name: 'testTodo1' },
    { name: 'testTodo2' },
    { name: 'testTodo3' },
  ]);

  public getTodos(): ITodoItem[] {
    return this.todos.getValue();
  }

  public getTodosSubscription(next: () => any, error: () => any) {
    return this.todos.subscribe({
      next,
      error
    });
  }

  public toggleTodo(index: number) {
    const f = this.todos.getValue();
    f[index].isDone = !f[index].isDone;
    this.todos.next([...f]);
  }

  public addTodo(todo: ITodoItem): void {
    this.todos.next([...this.todos.getValue(), todo]);
  }

  public removeTodo(index: number): void {
    const f = this.todos.getValue();
    f.splice(index, 1);
    this.todos.next([...f]);
  }

}
