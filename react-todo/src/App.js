import React from "react";
import AppTitle from "./components/AppTitle";
import TodoList from "./containers/TodoList";

function App() {
  return (
    <>
      <AppTitle />
      <TodoList />
    </>
  );
}

export default App;
