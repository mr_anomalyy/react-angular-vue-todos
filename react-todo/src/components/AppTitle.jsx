import React from "react";

export default class AppTitle extends React.PureComponent {
  render() {
    return (
      <div className="appTitle">
        <div className="title">React Todo</div>
        <div className="subtitle">from react-angular-vue-todos</div>
      </div>
    );
  }
}