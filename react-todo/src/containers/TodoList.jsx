import React from "react";
import { connect } from "react-redux";
import { addTodo, removeTodo, toggleTodo } from "../actions/todo";

class TodoList extends React.PureComponent {
  constructor() {
    super();
    this.handleAddTodo = this.handleAddTodo.bind(this);
    this.handleRemoveTodo = this.handleRemoveTodo.bind(this);
  }

  renderTodos() {
    if (!this.props.todos.length) {
      return (
        <div className="noTodos">Nothing Todo. :C</div>
      );
    }

    return this.props.todos.map((val, i) => this.renderTodoItem(val, i))
  }

  renderTodoItem(val, i) {
    return (
      <div key={i} className={val.isDone ? "todo completed" : "todo"}>
        <span onClick={this.handleToggleTodo(i)}>
          <span className="order">{i + 1}. </span>
          <span className="name">{val.name}</span>
        </span>
        <span onClick={this.handleRemoveTodo(i)} className="deleteButton">x</span>
      </div>
    );
  }

  handleToggleTodo(index) {
    return (_event) => {
      this.props.toggleTodo(index);
    }
  }

  handleRemoveTodo(index) {
    return (_event) => {
      if (window.confirm("Are you sure?")) {
        this.props.removeTodo(index);
      }
    }
  }

  handleAddTodo() {
    const todo = window.prompt("Enter your todo name");
    if (!todo || !todo.length) {
      return;
    }

    this.props.addTodo({ name: todo });
  }

  renderAddTodoButton() {
    return (
      <button onClick={this.handleAddTodo} className="addTodoButton">Add new Todo</button>
    );
  }

  render() {
    return (
      <div className="todoList">
        <div className="list">{this.renderTodos()}</div>
        {this.renderAddTodoButton()}
      </div>
    );
  }
}

const mapStateToProps = (store) => {
  return { todos: store.todoListReducer.todos }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addTodo: (todo) => dispatch(addTodo(todo)),
    removeTodo: (index) => dispatch(removeTodo(index)),
    toggleTodo: (index) => dispatch(toggleTodo(index))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(TodoList);