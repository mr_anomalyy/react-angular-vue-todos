import { 
  ADD_TODO, REMOVE_TODO, TOGGLE_TODO
} from "../constants/todo";

const initialState = {
  todos: [
    { name: "exTodo1" },
    { name: "exTodo2" },
    { name: "exTodo3" },
  ]
};

export default function todoListReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_TODO:
      return {
        ...state,
        todos: [...state.todos, action.payload] 
      }
    case REMOVE_TODO:
      state.todos.splice(action.payload, 1);
      return {
        ...state,
        todos: [...state.todos]
      }
    case TOGGLE_TODO:
      state.todos[action.payload].isDone = !state.todos[action.payload].isDone;
      return {
        ...state,
        todos: [...state.todos]
      }
        
    default:
      return state;
  }
};