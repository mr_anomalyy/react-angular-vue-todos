import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex';
import { ADD_TODO, REMOVE_TODO, TOGGLE_TODO } from './constants/base.js';

Vue.config.productionTip = false

Vue.use(Vuex);
const store = new Vuex.Store({
  state: {
    todos: [
      { name: "exTodo1" },
      { name: "exTodo2" },
      { name: "exTodo3" },
    ]
  },
  mutations: {
    [ADD_TODO] (state, payload) {
      state.todos = [...state.todos, payload];
    },
    [REMOVE_TODO] (state, index) {
      const f = state.todos;
      f.splice(index, 1);
      state.todos = f;
    },
    [TOGGLE_TODO] (state, index) {
      const f = state.todos;
      f[index].isDone = !f[index].isDone;
      state.todos = [...f];
    }
  },
  getters: {
    todos: (state) => state.todos
  }
})

new Vue({
  render: h => h(App),
  store
}).$mount('#app')